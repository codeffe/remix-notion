
import { Fragment } from 'react';
import { Link, useLoaderData, MetaFunction, LoaderFunction } from "remix";
import SITE from "../../../site.config.js";
import { getAllPosts } from "../../utils/notion";
import BlogPost from "../../components/BlogPost";

export async function loader() {
  const posts = await getAllPosts({ includePages: true });
  const topic = posts.filter((v) => v.topic === "Yes");
  const articles = posts.filter((v) => v.topic === "No");
  const page = posts.filter((v) => v.type.includes("Page"));
  const postsToShow = articles.slice(0, SITE.postsPerPage);
  const p1 = articles.slice(0, 1);
  const totalPosts = posts.length;
  const showNext = totalPosts > SITE.postsPerPage;
  console.log("loader-----", p1);
  return postsToShow

  const data = [
    {
      id: 1,
      title: 'Blogggg',
      summary: 'Posts',
      slug: '2022-weekly-3',
    }
  ];
  return data;
}

export default function Blog() {
  let posts = useLoaderData();
  return (
    <Fragment>
      {/* <h2 className="text-xl pb-4 ztitle">专题期刊</h2>
        <div className="grid gap-6 lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-1 mb-10">
          {topic.map((post) => (
            <BlogPost key={post.id} post={post} type={"topic"} />
          ))}
        </div>
        <h2 className="text-xl ztitle">技术周刊</h2>
        <div className="grid gap-6 lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-1">
          {postsToShow.map((post) => (
            <BlogPost key={post.id} post={post} type={"post"} />
          ))}
        </div> */}
      <div className="max-w-2xl mx-auto">
        <h2 className="text-2xl text-center">list</h2>
        {posts.map((post) => (
          // <BlogPost key={post.id} post={post} type={"post"} />
          <Link to={post.slug} key={post.id}>
            <div className="p-4 hover:text-gray-100 hover:bg-black transition">
              <h2 className="text-xl py-2">{post.title}</h2>
              <p>{post.summary}</p>
            </div>
          </Link>
        ))}
        {/* <Link to="2022-weekly-3">
          <h2>
            Param: <i>2022-weekly-3</i>
          </h2>
        </Link> */}
      </div>
    </Fragment>
  );
}