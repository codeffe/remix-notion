import { useCatch, Link, json, useLoaderData } from "remix";
import type { LoaderFunction, MetaFunction } from "remix";
// import { createHash } from "crypto";
import {
  NotionRenderer,
  Equation,
  Code,
  Collection,
  CollectionRow,
} from "react-notion-x";

import SITE from "../../../site.config.js";
import { getAllPosts, getPostBlocks } from "../../utils/notion";

import PageView from "../../components/page";

const mapPageUrl = (id: string) => {
  return "https://www.notion.so/" + id.replace(/-/g, "");
};

export let loader: LoaderFunction = async ({ params }) => {
  const posts = await getAllPosts({ includePages: true });
  const post = posts.find((v: any) => v.slug === params.postId);
  const blockMap = await getPostBlocks(post.id);
  // const emailHash = createHash("md5")
  //   .update(SITE.email)
  //   .digest("hex")
  //   .trim()
  //   .toLowerCase();
  return {
    param: params.postId,
    blockMap: blockMap,
    post: post,
    // emailHash: emailHash,
  };
};

export default function ParamDemo() {
  let data = useLoaderData();
  return (
    <div className="m-auto flex-grow w-full transition-all max-w-screen-lg px-4">
      <article>
        <h1 className="font-bold text-3xl text-black dark:text-white">
          {data.post.title}
        </h1>
        <NotionRenderer
          recordMap={data.blockMap}
          mapPageUrl={mapPageUrl}
          components={{
            equation: Equation,
            code: Code,
            collection: Collection,
            collectionRow: CollectionRow,
          }}
        />
      </article>
      <div className="flex justify-between font-medium text-gray-500 dark:text-gray-400">
        <a>
          <button
            onClick={() => window.scrollTo({ top: 0, behavior: "smooth" })}
            className="mt-2 cursor-pointer hover:text-black dark:hover:text-gray-100"
          >
            ↑ {`TOP`}
          </button>
        </a>
      </div>
    </div>
  );
}

// https://remix.run/api/conventions#catchboundary
// https://remix.run/api/remix#usecatch
// https://remix.run/api/guides/not-found
export function CatchBoundary() {
  let caught = useCatch();

  let message: React.ReactNode;
  switch (caught.status) {
    case 401:
      message = (
        <p>
          Looks like you tried to visit a page that you do not have access to.
          Maybe ask the webmaster ({caught.data.webmasterEmail}) for access.
        </p>
      );
    case 404:
      message = (
        <p>Looks like you tried to visit a page that does not exist.</p>
      );
    default:
      message = (
        <p>
          There was a problem with your request!
          <br />
          {caught.status} {caught.statusText}
        </p>
      );
  }

  return (
    <>
      <h2>Oops!</h2>
      <p>{message}</p>
      <p>
        (Isn't it cool that the user gets to stay in context and try a different
        link in the parts of the UI that didn't blow up?)
      </p>
    </>
  );
}

// https://remix.run/api/conventions#errorboundary
// https://remix.run/api/guides/not-found
export function ErrorBoundary({ error }: { error: Error }) {
  console.error(error);
  return (
    <>
      <h2>Error!</h2>
      <p>{error.message}</p>
      <p>
        (Isn't it cool that the user gets to stay in context and try a different
        link in the parts of the UI that didn't blow up?)
      </p>
    </>
  );
}
