import { Fragment } from "react";
import { useLoaderData } from "remix";
import type { LoaderFunction } from "remix";

export async function loader() {
  let res = await fetch("https://api.github.com/gists");
  return res.json();
}


export default function About() {
  let gists = useLoaderData();
  return (
    <div className="mx-auto transition-all max-w-screen-md">
      <h1 className="text-center">Welcome to Remix</h1>
      <ul>
        {gists.slice(0, 5).map((gist) => (
          <li>
            <a href={gist.html_url}>{gist.id}</a>
          </li>
        ))}
      </ul>
    </div>
  );
}
