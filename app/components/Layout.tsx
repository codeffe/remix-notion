import { Fragment } from "react";
import Nav from "./Nav";
import Footer from "./Footer";

export default function Layout({ children }: { children: React.ReactNode }) {
  return (
    <Fragment>
      <Nav />
      <main className="m-auto flex-grow w-full transition-all mb-12">{children}</main>
      <Footer />
    </Fragment>
  );
}
