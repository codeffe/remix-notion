import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration
} from "remix";
import type { MetaFunction } from "remix";

import prism from"prismjs/themes/prism.css";
import notionX from "react-notion-x/src/styles.css";
import katex from "katex/dist/katex.min.css";
import global from "~/styles/global.css";
import tailwind from "~/styles/tailwind.css";
import notion from "~/styles/notion.css";

import Layout from "~/components/Layout";

export function links() {
  return [
    { rel: "stylesheet", href: prism },
    { rel: "stylesheet", href: notionX },
    { rel: "stylesheet", href: katex },
    { rel: "stylesheet", href: global },
    { rel: "stylesheet", href: tailwind },
    { rel: "stylesheet", href: notion },
  ];
}

export const meta: MetaFunction = () => {
  return { title: "Remix Tailwinds Start" };
};

export default function App() {
  return (
    <Document>
      <Layout>
        <Outlet />
      </Layout>
    </Document>
  );
}

export function ErrorBoundary({ error }: { error: Error }) {
  console.error(error);
  return (
    <Document title="Error!">
      <Layout>
        <div>
          <h1>There was an error</h1>
          <p>{error.message}</p>
        </div>
      </Layout>
    </Document>
  );
}

function Document({
  children,
  title,
}: {
  children: React.ReactNode;
  title?: string;
}) {
  return (
    <html lang="en">
      <head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width,initial-scale=1" />
        {title ? <title>{title}</title> : null}
        <Meta />
        <Links />
      </head>
      <body>
        {children}
        <ScrollRestoration />
        <Scripts />
        {process.env.NODE_ENV === "development" && <LiveReload />}
      </body>
    </html>
  );
}
